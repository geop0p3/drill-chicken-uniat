extends Node2D

onready var scene_luna = load("res://Luna.tscn")
var sprites_luna = [load ("res://Images/Asteroide.png"), load ("res://Images/Luna purple.png"), load ("res://Images/Luna Verde.png"),load("res://Images/Luna.png")]
onready var scene_planeta = load("res://Planeta.tscn")
var width

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	width = get_viewport_rect().size.x
	randomize()
	var sprite_chooser = rand_range(0,3)
	var y = 0
	while y > -3000:
		var nueva_luna = scene_luna.instance()
		sprite_chooser = rand_range(0,3)
		nueva_luna.set_position(Vector2(rand_range(-width/2, width/2), y))
		var sprite = nueva_luna.get_node("Sprite")
		sprite.texture = sprites_luna[sprite_chooser]
		add_child(nueva_luna)
		y -= rand_range(0,220)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
