extends RigidBody2D

onready var audio_player = get_node("AudioStreamPlayer")
onready var audio_player2 = get_node("AudioStreamPlayer2")
onready var timer = get_node("Timer")
onready var timer2 = get_node("Timer2")
onready var timer3 = get_node("Timer3")
var can_launch = true;
var direccion = 0
var drilling = false;
var current_collision;
var current_planet;
var current_planetref;
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

#warning-ignore:unused_argument
func _process(delta):
	current_planetref = weakref(current_planet)
	if (drilling == true):
		$CollisionShape2D/AnimatedSprite.play("Drilling")
	if (drilling == false):
		$CollisionShape2D/AnimatedSprite.play("Idle")
	if (global.current_planet != null):
		direccion = get_angle_to(global.current_planet.position)
		self.angular_velocity = direccion
	else:
		self.angular_velocity = 0
		

func _input(event):
	print(current_planet)
	if (global.on_planet):
		if (event.is_action_pressed("launch") or InputEventScreenTouch):
			if (can_launch):
				if current_collision != null:
					current_collision.disabled = true;
					timer3.start(1)
				drilling = true;
				print("Drilling!")
				audio_player2.play(0);
				apply_central_impulse(Vector2(0,-200).rotated(-direccion))
				timer.start(1.5)
				timer2.start(1.5)
				can_launch = false;


func _on_Area2D_area_entered(area):
	print (area)
	if (area.is_in_group("Moneda")):
		destroy_moneda(area)
		
	if (area.is_in_group("Luna")):
		current_planet = area;
		drilling = false;
		self.angular_damp = 10
		global.on_planet = true;
		global.current_planet = area
		can_launch = true;
		current_collision = area.get_node("StaticBody2D/CollisionShape2D")
		print("Luna")
		
	if (area.is_in_group("Planeta")):
		current_planet = area;
		drilling = false;
		self.angular_damp = 10
		global.current_planet = area;
		global.on_planet = true;
		can_launch = true;
		global.on_planet = true;
		current_collision = area.get_node("StaticBody2D/CollisionShape2D")
		print("Planeta")

func _on_Area2D_area_exited(area):
	if (area.is_in_group("Luna")):
		global.on_planet = false;
		global.current_planet = null;
		print("Luna")
		
	if (area.is_in_group("Planeta")):
		global.on_planet = false;
		global.current_planet = null;
		print("Planeta")

func destroy_moneda(moneda):
	print("Moneda")
	global.score += 100;
	audio_player.play(0);
	var texto = get_node("../Camera2D2/Label_Score/Score");
	texto.text = str(global.score);
	moneda.queue_free();

func _on_Timer_timeout():
	can_launch = true;


func _on_Timer2_timeout():
	drilling = false;
	pass # Replace with function body.


func _on_Timer3_timeout():
	if current_planetref.get_ref():
		current_planet.queue_free()
	pass # Replace with function body.
